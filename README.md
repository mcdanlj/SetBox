Our game of [Set](https://www.amazon.com/SET-Family-Game-Visual-Perception/dp/B00000IV34)
fell and the box broke, but the lid survived. This is a replacement
that is designed to interface with the original lid. I don't know
how consistent the game has been over time, so I don't know whether
this would work for others.

This is made with [FreeCAD LinkStage3](https://github.com/realthunder/FreeCAD_assembly3/releases)
with save to directory. The SetBox.FCstd file may work with upstream
FreeCAD but the SetBox directory is the canonical location.
